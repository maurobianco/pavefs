#include <cassert>
#include "to_compact.hpp"
#include <iostream>

template <typename T>
bool test(T x, std::string s) {
	std::cout << "convert " << x << " to " << to_compact(x) << " (expecting " << s << ")\n";
	return to_compact(x) == s;
}

int main() {
	assert(test(1000, std::string("1KBi")));
	assert(test(10000, std::string("10KBi")));
	assert(test(998, std::string("998B")));
	assert(test(1023, std::string("1KBi")));
	assert(test(1025,  std::string("1KBi")));
	assert(test((1<<20)+1,  std::string("1MBi")));
	assert(test((1<<30),  std::string("1GBi")));
	assert(test(150ull*((1ull)<<30), std::string("161GBi")));
}
#include <thread>
#include <condition_variable>
#include <mutex>
#include <iostream>
#include <cassert>
#include <atomic>

#include "tree_node.hpp"

void visit_fs(tree_node_t &, bool);

std::mutex cv_m;
std::mutex m2;

int n_threads;
std::vector<tree_node_t *> nodes_to_process;
std::vector<std::condition_variable> conds;
std::vector<std::thread> threads;
std::vector<bool> free_thread;
std::atomic<bool> still_working = true;
std::atomic<int> n_workers = 0;

void worker(int index)
{
    while (still_working)
    {
        {
            std::unique_lock<std::mutex> lk(cv_m);
            if (nodes_to_process[index] == nullptr) {
                conds[index].wait(lk, [index] {
                    return nodes_to_process[index] != nullptr || !still_working;
                });
            }
        }
        if (!still_working) return;
        visit_fs(*(nodes_to_process[index]), true);

        {
            std::lock_guard<std::mutex> lock(m2);
            nodes_to_process[index] = nullptr;
            free_thread[index] = true;
            n_workers--;
        }
    }
}

void init_thread_poll(int nt)
{
    n_threads = nt;
    nodes_to_process = std::vector<tree_node_t *>(n_threads, nullptr);
    conds = std::vector<std::condition_variable>(n_threads);
    free_thread = std::vector<bool>(n_threads, true);
    for (int i = 0; i < n_threads; ++i)
    {
        threads.push_back(std::thread{worker, i});
    }
}

int get_thread()
{
    std::lock_guard<std::mutex> lock(m2);
    int i = 0;
    while (still_working and i<n_threads and !free_thread[i]) {++i;}

    if (i == n_threads || still_working == false)
    {
        return -1;
    }
    else
    {
        free_thread[i] = false;
        n_workers++;
        return i;
    }
}

void run_visit_on(tree_node_t &tree)
{
    int th_id = get_thread();
    if (th_id == -1)
    {
        visit_fs(tree, true);
    }
    else
    {
        assert(th_id >= 0 && th_id < n_threads);
        assert(nodes_to_process[th_id] == nullptr);
        {
            std::unique_lock<std::mutex> lk(cv_m);

            nodes_to_process[th_id] = &tree;
        }
        conds[th_id].notify_one();
    }
}

void join_all()
{
    while (n_workers > 0) { }
    still_working = false;
    for (int i = 0; i < n_threads; ++i)
    {
        conds[i].notify_one();
    }
    for (int i = 0; i < n_threads; ++i)
    {
        threads[i].join();
    }
}

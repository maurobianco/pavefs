#include <string>
#include <nlohmann/json.hpp>
#include "tree_node.hpp"

using json = nlohmann::json;

void fs_from_json(tree_node_t& tree, json const& j);
json populate_json(tree_node_t const &tree);

void read_from_json(tree_node_t &tree, std::string const& fname);
void write_json(tree_node_t const& tree, std::string const& fname);
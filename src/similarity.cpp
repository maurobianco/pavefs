#include <tree_node.hpp>

bool similar_structure(tree_node_t const &node1, tree_node_t const &node2)
{
  if (node1.sons.empty() and node2.sons.empty())
  {
    return true;
  }
  if (node1.sons.size() != node2.sons.size())
  {
    return false;
  }

  bool check = true;
  auto it1 = node1.sons.begin();
  auto it2 = node2.sons.begin();
  for (; it1 != node1.sons.end(); ++it1, ++it2)
  {
    check = check and similar_structure(*it1, *it2);
  }
  return check;
}

void nodes_with_similar_structure(tree_node_t const &tree, tree_node_t const &sample, const int heingt, std::list<tree_node_t *const> &list)
{
  if (tree.height == heingt)
  {
    if (similar_structure(tree, sample))
    {
      list.push_back(const_cast<tree_node_t *const>(&tree));
    }
    return;
  }
  if (tree.height < heingt)
  {
    return;
  }
  if (!tree.sons.empty())
  {
    for (auto &t : tree.sons)
    {
      if (!t.sons.empty())
      { // must be a folder
        nodes_with_similar_structure(t, sample, heingt, list);
      }
    }
  }
}

void search_similar(tree_node_t const &tree, tree_node_t const *node, std::list<tree_node_t *const> &list)
{
  // similar folders shouold have the same height
  nodes_with_similar_structure(tree, *node, node->height, list);
}

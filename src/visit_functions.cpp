#include <algorithm>
#include <cassert>
#include <iostream>

#include <filesystem>
#include <SFML/Graphics.hpp>

#include "tree_node.hpp"
#include "thread_pool.hpp"

std::string n_char(char c, int n)
{
    std::string s;
    for (int i = 0; i < n; ++i)
        s += c;
    return s;
}

void visit_fs(tree_node_t &tree, bool using_thread_pool = false)
{
    if (fs::is_directory(tree.file))
    {
        for (fs::directory_entry const &x : fs::directory_iterator(tree.file))
        {
            if (!(x.path().filename() == fs::path(".")) and !(x.path().filename() == fs::path("..")))
            {
                auto &y = tree.sons.emplace_back(x.path());
                try
                {
                    if (fs::exists(x.path()) && fs::is_regular_file(x.path()))
                    {
                        y.size = file_size(x.path());
                    }
                    if (fs::is_directory(x.path()) and !fs::is_symlink(x.path()))
                    {
                        if (using_thread_pool)
                            run_visit_on(y);
                        else
                        {
                            visit_fs(y, using_thread_pool);
                        }

                    }
                }
                catch (...)
                {
                }
            }
        }
    }
}

size_t update_size(tree_node_t &tree)
{
    if (!tree.sons.empty())
    {
        int h = 0;
        for (auto &x : tree.sons)
        {
            tree.size += update_size(x);
            h = (h > x.height) ? h : x.height;
        }
        tree.height = h + 1;
        return tree.size;
    }
    else
    {
        tree.height = 0;
        return tree.size;
    }
}

sf::Color color_from_extension(std::string const &fname)
{
    //std::cout << "fname " << fname << "\n";
    size_t n = fname.size();
    if (n >= 3)
    {
        return sf::Color{(((unsigned int)fname[n - 1]) << 24) + (((unsigned int)fname[n - 2]) << 16) + (((unsigned int)fname[n - 3]) << 8) + 255};
    }
    else
    {
        return sf::Color::Red;
    }
}

void sort_all_lists(tree_node_t &tree)
{
    if (tree.sons.empty())
        return;

    tree.sons.sort([](tree_node_t const &a, tree_node_t const &b) { return a.size > b.size; });

    for (auto &x : tree.sons)
    {
        sort_all_lists(x);
    }
}

void update_rect(tree_node_t &tree /*, sf::Rect<double> const& total_rect*/)
{

    if (tree.sons.empty())
        return;

    tree.sons.sort([](tree_node_t const &a, tree_node_t const &b) { return a.size > b.size; });

    double a = tree.rect.width, b = tree.rect.height;
    double x = tree.rect.left, y = tree.rect.top;

    auto it = tree.sons.begin();
    double original_area = a * b;

    while (it != tree.sons.end())
    {
        assert(it->size <= tree.size);
        if (tree.size == 0 || a == 0. || b == 0.)
        {
            it->rect = sf::Rect(x, y, 0., 0.);
            it->color = color_from_extension(it->file.filename().string());
        }
        else
        {
            double file_area = original_area * ((double)it->size / (double)tree.size);
            if (a > b)
            {
                assert(b != 0.);
                double a0 = file_area / b;
                it->rect = sf::Rect(x, y, a0, b);
                it->color = color_from_extension(it->file.filename().string());
                a = a - a0;
                x = x + a0;
            }
            else
            {
                assert(a != 0.);
                double b0 = file_area / a;
                it->rect = sf::Rect(x, y, a, b0);
                it->color = color_from_extension(it->file.filename().string());
                b = b - b0;
                y = y + b0;
            }
        }
        update_rect(*it);
        ++it;
    }
}

void update_rect_v2(tree_node_t::iterator_t begin, tree_node_t::iterator_t end, size_t list_lenght, size_t folder_size, sf::Rect<double> const &total_rect)
{

    if (begin == end)
    {
        assert(list_lenght == 0);
    }

    if (list_lenght == 1)
    {
        begin->rect = total_rect;
        begin->color = color_from_extension(begin->file.filename().string());
        if (!begin->sons.empty())
        {
            update_rect_v2(begin->sons.begin(), begin->sons.end(), begin->sons.size(), begin->size, total_rect);
        }
    }
    else
    {

        double a = total_rect.width, b = total_rect.height;
        double x = total_rect.left, y = total_rect.top;

        double original_area = a * b;

        auto mid_it = begin;
        size_t mid_list_lenght = 0;
        size_t mid_size = 0;
        for (int i = 0; i < list_lenght / 2; ++i, ++mid_it)
        {
            mid_size += mid_it->size;
            ++mid_list_lenght;
        }

        sf::Rect<double> left_rect;
        sf::Rect<double> right_rect;

        if (folder_size == 0)
        {
            left_rect = sf::Rect(x, y, 0., 0.);
            right_rect = sf::Rect(x, y, 0., 0.);
        }
        else
        {
            double mid_area = original_area * ((double)mid_size / (double)folder_size);
            if (a > b)
            {
                double a0 = mid_area / b;
                double a1 = a - a0;
                left_rect = sf::Rect(x, y, a0, b);
                right_rect = sf::Rect(x + a0, y, a1, b);
            }
            else
            {
                double b0 = mid_area / a;
                double b1 = b - b0;
                left_rect = sf::Rect(x, y, a, b0);
                right_rect = sf::Rect(x, y + b0, a, b1);
            }
        }
        update_rect_v2(begin, mid_it, mid_list_lenght, mid_size, left_rect);
        update_rect_v2(mid_it, end, list_lenght - mid_list_lenght, folder_size - mid_size, right_rect);
    }
}

void print_fs(tree_node_t const &tree, int lvl = 0)
{
    if (!tree.sons.empty())
    {
        for (auto &x : tree.sons)
        {
            std::cout << n_char('\t', lvl) << x.file << " (" << x.size << ") -- "
                      << "[ top = " << x.rect.top
                      << ", left = " << x.rect.left
                      << ", width = " << x.rect.width
                      << ", height = " << x.rect.height
                      << "]\n";
            print_fs(x, lvl + 1);
        }
    }
}

#include <iostream>
#include <algorithm>
#include <cassert>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <SFML/Graphics.hpp>

#include <nlohmann/json.hpp>

#include "thread_pool.hpp"

#include "timer.hpp"
#include "tree_node.hpp"
#include "json_io.hpp"

#include "visit_functions.hpp"
#include "get_file_at.hpp"
#include "similarity.hpp"


using json = nlohmann::json;


int main(int argc, char *argv[])
{

  json jstruct;

  bool only_analysis = false;
  std::string fname{"pavefs.json"};
  std::string path;
  int n_threads;

  boost::program_options::options_description desc("Usage");
  desc.add_options()
  ("analysis,a", "If specified it does analysis and writes into json file\n")
  ("outout,o", boost::program_options::value<std::string>(&fname), "json file to write to\n")
  ("input,i", boost::program_options::value<std::string>(&fname), "json file to read from\n")
  ("path,p", boost::program_options::value<std::string>(&path)->default_value("."), "Path to analyze\n")
  ("threads,t", boost::program_options::value<int>(&n_threads)->default_value(0), "Number of odditional threads to use (0 will not use thread pool at all)\n")
  ("help,?", "Produce help\n");

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);

  if (vm.count("analysis")) {
    only_analysis = true;
  }

  if (vm.count("input") && vm.count("output")) {
    std::cerr << "\nError: Only one of input/output can be sopecified in the command line\n\n";
    std::cerr << desc << "\n";
    std::terminate();
  }

  if (vm.count("analysis") && vm.count("input")) {
    std::cerr << "\nError: --input and --analysis cannot be specified at the same time: Analysis can only be done on a path\n\n";
    std::cerr << desc << "\n";
    std::terminate();
  }

  if (vm.count("help"))
  {
    std::cout << "pavefs Copyright (C) 2020  Mauro Bianco\n";
    std::cout << "This program comes with ABSOLUTELY NO WARRANTY\n";
    std::cout << "See <http://www.gnu.org/licenses/> for liocensing information\n";
    std::cout << "This is free software, and you are welcome to redistribute it\n";
    std::cout << "under certain conditions;\n";
    std::cout << desc << "\n";
    return 1;
  }

  fs::path p(path);

  if (n_threads > 0)
    init_thread_poll(n_threads);

  tree_node_t tree;

  int win_x = 1200, win_y = 900;

  try
  {
    if (vm.count("input")) {
      timer::tic();
      read_from_json(tree, fname);
      //return 0;

    } else {
      if (fs::exists(p) and fs::is_directory(p))
      {
        tree = tree_node_t{p};
        timer::tic();

        if (n_threads > 0)
        {
          visit_fs(tree, true);
          join_all();
        }
        else
        {
          visit_fs(tree, false);
        }

        std::cout << "VisitFS: " << timer::toc() << "s\n";

        update_size(tree);

        std::cout << "Update sizes: " << timer::toc() << "s\n";

        tree.rect = sf::Rect<double>{0., 0., (double)win_x, (double)win_y};
        sort_all_lists(tree);
        update_rect_v2(tree.sons.begin(), tree.sons.end(), tree.sons.size(), tree.size, tree.rect);

        std::cout << "Compute rects: " << timer::toc() << "s\n";
        //print_fs(tree);

        if (only_analysis)
        {
          write_json(tree, fname);
          return 0;
        }
      }
    }
  }
  catch (const fs::filesystem_error &ex)
  {
    std::cout << ex.what() << '\n';
    std::terminate();
  }

  sf::RenderTexture texture;
  if (!texture.create(win_x, win_y))
  {
    std::terminate();
  }

  sf::RenderWindow window(sf::VideoMode(win_x + 3, win_y + 3), "pavefs");

  sf::Font font;
  font.loadFromFile("./Inconsolata.ttf");

  sf::Text text;
  text.setOutlineThickness(0);
  text.setFillColor(sf::Color::Black);
  text.setFont(font);
  text.setStyle(sf::Text::Regular);
  text.setCharacterSize(16);

  sf::RectangleShape text_bg;
  text_bg.setFillColor(sf::Color{243, 238, 165, 255});
  text_bg.setOutlineThickness(1);
  text_bg.setOutlineColor(sf::Color::Red);

  tree_node_t *current_file = nullptr;
  tree_node_t *current_folder = nullptr;
  std::list<tree_node_t *const> similar_folders;
  bool show_similar = false;

  std::cout << "Intermediate stuff: " << timer::toc() << "s\n";
  draw_fs(tree, texture);
  texture.display();
  sf::Sprite sprite(texture.getTexture());
  sprite.setPosition(0.f, 0.f);
  std::cout << "Compute visualization: " << timer::toc() << "s\n";

  sf::Vector2i previous_pos{0, 0};

  sf::RectangleShape highlight_rect;
  highlight_rect.setOutlineColor(sf::Color::Yellow);
  highlight_rect.setOutlineThickness(2);
  highlight_rect.setFillColor(sf::Color::Transparent);
  int highlight_height = 1;

  sf::RectangleShape similar_rect;
  similar_rect.setOutlineColor(sf::Color::Green);
  similar_rect.setOutlineThickness(2);
  similar_rect.setFillColor(sf::Color::Transparent);

  while (window.isOpen())
  {
    // Process events
    sf::Event event;
    while (window.pollEvent(event))
    {
      // Close window: exit
      if (event.type == sf::Event::Closed)
        window.close();
      if (event.type == sf::Event::KeyPressed)
      {
        switch (event.key.code)
        {
        case sf::Keyboard::Escape:
          window.close();
          break;
        case sf::Keyboard::RBracket:
          if (highlight_height < tree.height)
          {
            ++highlight_height;
          }
          break;
        case sf::Keyboard::LBracket:
          if (highlight_height > 1)
          {
            --highlight_height;
          }
          break;
        case sf::Keyboard::Tab:
          if (show_similar == false)
          {
            if (current_folder != nullptr and highlight_height >= 2)
            {
              assert(current_file != nullptr);
              similar_folders.clear();
              search_similar(tree, current_folder, similar_folders);
              show_similar = true;
            }
          }
          else
          {
            show_similar = false;
          }
          break;
        default:
          break;
        }
      }
    }

    window.clear();

    window.draw(sprite);

    if (show_similar)
    {
      for (auto t : similar_folders)
      {
        similar_rect.setPosition(sf::Vector2f{(float)t->rect.left, (float)t->rect.top});
        similar_rect.setSize(sf::Vector2f{(float)t->rect.width, (float)t->rect.height});
        window.draw(similar_rect);
      }
    }

    auto pos = sf::Mouse::getPosition(window);
    if (pos.x >= 0 and pos.y >= 0 and pos.x < win_x and pos.y < win_y)
    {
      std::string file = get_file_at(tree, pos, highlight_rect, highlight_height, current_file, current_folder);
      window.draw(highlight_rect);
      text.setString(file + " -- " + std::to_string(highlight_height));
      auto bounds = text.getLocalBounds();
      if (pos != previous_pos)
      {
        float shift = 0.f;
        if (pos.x + bounds.width > win_x)
        {
          shift = (pos.x + bounds.width) - win_x;
        }
        text_bg.setPosition(static_cast<sf::Vector2f>(pos) - sf::Vector2f{shift + 2, 30.f});
        text_bg.setSize({bounds.width + 6, bounds.height + 4});
        text.setPosition(static_cast<sf::Vector2f>(pos) - sf::Vector2f{shift, 30.f});
        previous_pos = pos;
      }
      window.draw(text_bg);
      window.draw(text);
    }

    window.display();
  }

return 0;
}

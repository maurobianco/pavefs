namespace timer {
  void tic();
  double toc();
}

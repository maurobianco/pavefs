#include <chrono>

namespace timer {
  namespace {
    using now_t = decltype(std::chrono::steady_clock::now());
    now_t stamp;
  }

  void tic() {
    stamp = std::chrono::steady_clock::now();
  }

  double toc() {
    auto now = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = now - stamp;
    stamp = now;
    return elapsed_seconds.count();
  }
}

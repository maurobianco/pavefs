#include <utility>
#include <type_traits>
#include <string>
#include <iostream>
#include <fstream>
#include <SFML/Graphics/Rect.hpp>

#include <nlohmann/json.hpp>
#include "timer.hpp"
#include "tree_node.hpp"

using json = nlohmann::json;

void fs_from_json(tree_node_t &tree, json const &j)
{
    tree.file = fs::path(j["node"]["file"]);
    tree.height = j["node"]["height"];
    tree.size = j["node"]["size"];
    tree.color = sf::Color{j["node"]["color"]};
    double r[4];
    int i = 0;
    for (auto v : j["node"]["rect"])
    {
        r[i++] = v;
    }
    tree.rect = sf::Rect<double>{r[0], r[1], r[2], r[3]};
    if (j["node"].find("sons") == j["node"].end())
    {
        return;
    }
    for (auto v : j["node"]["sons"])
    {
        tree_node_t t;
        fs_from_json(t, v);
        tree.sons.push_back(t);
    }
}

json populate_json(tree_node_t const &tree)
{
    /*
    fs::path file;
    size_t size = 0;
    int height = -1; // height of the node (-1 is unset, leaf is height == 0)
    container_t sons;
    sf::Rect<double> rect;
    sf::Color color;
    */
    json j;
    j["node"]["file"] = tree.file.string();
    j["node"]["size"] = tree.size;
    j["node"]["height"] = tree.height;
    j["node"]["color"] = tree.color.toInteger();
    j["node"]["rect"] = {tree.rect.left, tree.rect.top, tree.rect.width, tree.rect.height};
    if (!tree.sons.empty())
    {
        for (auto &x : tree.sons)
        {
            j["node"]["sons"].push_back(populate_json(x));
        }
    }
    return j;
}

void read_from_json(tree_node_t &tree, std::string const &fname)
{
    std::fstream infile{fname, infile.in};

    std::string s;

    infile.seekg(0, std::ios::end);
    s.reserve(infile.tellg());
    infile.seekg(0, std::ios::beg);

    s.assign((std::istreambuf_iterator<char>(infile)),
             std::istreambuf_iterator<char>());

    std::cout << "Read file "
              << timer::toc() << "\n";
    auto j = json::parse(s);

    fs_from_json(tree, j);
    std::cout << "Parsing and filling up tree " << timer::toc() << "s\n";
}

void write_json(tree_node_t const &tree, std::string const &fname)
{
    json j = populate_json(tree);
    std::cout << "Json populated " << timer::toc() << "s\n";
    auto jtext = j.dump(2);
    std::cout << "Json dumped " << timer::toc() << "s\n";
    std::fstream outfile{fname, outfile.out};

    outfile << jtext << "\n";
    std::cout << "Done " << timer::toc() << "s\n";
}
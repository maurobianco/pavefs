
#include <string>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "tree_node.hpp"
#include "to_compact.hpp"

/**
  * Helper funciton to retrieve the file where the mouse is and to highlight the folder contianing it.
  * The parameter hheight (ranging from 1 to the height of the tree) will determine how many levels
  * above the file to highlight (1 == the contianing folder, 2 == the folder containing the containing
  * folder, etc.)
  *
  * @param tree       The directory tree
  * @param pos        The position of the mouse over the files
  * @param rect       The reference to the shape that will be used to highligh the folder
  * @param current_h  The height of a folder in the current path - used to compare with hheight for highlighting
  * @param hheight    The number of levels up (first is 1) the highlighting will happen
  * @param was_drawn  Parameter used to control if the highlight happen, and if not the root is highlighted
  * @param max_height Parameter indicating the height of the root of the tree to check when we are back to the root (we could have used a static variable since the tree is always the same, but this seems more appropriate)
*/
std::string get_file_at_impl(tree_node_t const &tree, sf::Vector2i const &pos, sf::RectangleShape &rect,
                             int &current_h, int hheight, bool &was_drawn, const int max_height, tree_node_t *&node, tree_node_t *&folder)
{
  if (tree.sons.empty())
  {
    current_h = 0;
    node = const_cast<tree_node_t *>(&tree);
    return tree.file.string() + " (" + to_compact(tree.size);
  }
  else
  {
    auto it = tree.sons.begin();
    while (!it->rect.contains((static_cast<sf::Vector2<double>>(pos))) and it != tree.sons.end())
      ++it;
    if (it != tree.sons.end())
    {
      std::string s = get_file_at_impl(*it, pos, rect, current_h, hheight, was_drawn, max_height, node, folder);
      if (current_h == hheight)
      {
        rect.setSize(sf::Vector2f{(float)(*it).rect.width, (float)(*it).rect.height});
        rect.setPosition(sf::Vector2f{(float)(*it).rect.left, (float)(*it).rect.top});
        was_drawn = true;
        folder = const_cast<tree_node_t *>(&(*it));
        s = s + "/" + to_compact(folder->size) + ")";
      }
      if (was_drawn == false && max_height == tree.height)
      {
        rect.setSize(sf::Vector2f{(float)tree.rect.width, (float)tree.rect.height});
        rect.setPosition(sf::Vector2f{(float)tree.rect.left, (float)tree.rect.top});
        was_drawn = true;
        folder = const_cast<tree_node_t *>(&tree);
        s = s + "/" + to_compact(folder->size) + ")";
      }

      current_h++;
      return s;
    }
    else
    {
      return tree.file.string() + " not a file";
    }
  }
}

/**
  * Funciton to retrieve the file where the mouse is and to highlight the folder contianing it.
  * The parameter hheight (ranging from 1 to the height of the tree) will determine how many levels
  * above the file to highlight (1 == the contianing folder, 2 == the folder containing the containing
  * folder, etc.)
  *
  * @param tree     The directory tree
  * @param pos      The position of the mouse over the files
  * @param rect     The reference to the shape that will be used to highligh the folder
  * @param hheight  The number of levels up (first is 1) the highlighting will happen
*/
std::string get_file_at(tree_node_t const &tree, sf::Vector2i const &pos, sf::RectangleShape &rect, int hheight, tree_node_t *&node, tree_node_t *&folder)
{
  int current_height = -1;
  bool was_drawn = false;
  return get_file_at_impl(tree, pos, rect, current_height, hheight, was_drawn, tree.height, node, folder);
}

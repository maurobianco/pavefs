#include <list>
#include <tree_node.hpp>

bool similar_structure(tree_node_t const &node1, tree_node_t const &node2);

void nodes_with_similar_structure(tree_node_t const &tree, tree_node_t const &sample, const int heingt, std::list<tree_node_t *const> &list);

void search_similar(tree_node_t const &tree, tree_node_t const *node, std::list<tree_node_t *const> &list);

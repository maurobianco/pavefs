#include "tree_node.hpp"

/**
  * Funciton to retrieve the file where the mouse is and to highlight the folder contianing it.
  * The parameter hheight (ranging from 1 to the height of the tree) will determine how many levels
  * above the file to highlight (1 == the contianing folder, 2 == the folder containing the containing
  * folder, etc.)
  *
  * @param tree     The directory tree
  * @param pos      The position of the mouse over the files
  * @param rect     The reference to the shape that will be used to highligh the folder
  * @param hheight  The number of levels up (first is 1) the highlighting will happen
*/
std::string get_file_at(tree_node_t const &tree, sf::Vector2i const &pos, sf::RectangleShape &rect, int hheight, tree_node_t *&node, tree_node_t *&folder);
#ifndef __TO_COMPACT_HPP__
#define __TO_COMPACT_HPP__

#include <vector>
#include <string>

/** 
 * Translate a integer number reprensenting a file size in the most appropriate short
 * description with B/KBi/MBi/GBi
*/
template <typename SizeT>
std::string to_compact(SizeT size) {
  static std::vector<std::string> sizes = {"B", "KBi", "MBi", "GBi"};

  int i = 0;
  while (size/1000 > 0) {
    size = size/1000;
    ++i;
  }
  return std::to_string(size) + sizes[i];
}

#endif
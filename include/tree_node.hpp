#ifndef _TREE_NODE_HPP_
#define _TREE_NODE_HPP_

#include <list>
#include <filesystem>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Color.hpp>

namespace fs = std::filesystem;

struct tree_node_t
{
    using container_t = std::list<tree_node_t>; // The algorithm needs iterators that are valid
    using iterator_t = container_t::iterator;
    using const_iterator_t = container_t::const_iterator;

    fs::path file;
    size_t size = 0;
    int height = -1; // height of the node (-1 is unset, leaf is height == 0)
    container_t sons;
    sf::Rect<double> rect;
    sf::Color color;

    tree_node_t() = default;

    tree_node_t(fs::path const &p)
        : file{p}, sons{}
    {
    }

    bool is_valid() const
    {
        return !(size != 0 && file.empty());
    }

    tree_node_t &operator=(tree_node_t const &t)
    {
        file = t.file;
        sons = t.sons;
        return *this;
    }
};

#endif

#include <algorithm>
#include <cassert>

#include <boost/filesystem.hpp>
#include <SFML/Graphics/Rect.hpp>

std::string n_char(char c, int n);

void visit_fs(tree_node_t &tree, bool using_threading_pool);

size_t update_size(tree_node_t &tree);

sf::Color color_from_extension(std::string const &fname);

void sort_all_lists(tree_node_t &tree);

void update_rect(tree_node_t &tree /*, sf::Rect<double> const& total_rect*/);

void update_rect_v2(tree_node_t::iterator_t begin, tree_node_t::iterator_t end, size_t list_lenght, size_t folder_size, sf::Rect<double> const &total_rect);

void print_fs(tree_node_t const &tree, int lvl = 0);

template <typename RenderPlace>
void draw_fs(tree_node_t const &tree, RenderPlace &win)
{
    if (tree.sons.empty() or tree.rect.width < 1 or tree.rect.height <1 )
    {
        sf::RectangleShape r{sf::Vector2f{(float)tree.rect.width, (float)tree.rect.height}};
        r.setPosition(sf::Vector2f{(float)tree.rect.left, (float)tree.rect.top});
        r.setOutlineColor(sf::Color::White);
        r.setOutlineThickness(1.f);
        r.setFillColor(tree.color);
        //    r.setFillColor(sf::Color::Transparent);
        win.draw(r);
    }
    else
    {
        for (auto const &x : tree.sons)
        {
            // sf::RectangleShape r{sf::Vector2f{(float)tree.rect.width, (float)tree.rect.height}};
            // r.setPosition(sf::Vector2f{(float)tree.rect.left, (float)tree.rect.top});
            // r.setOutlineColor(sf::Color::Red);
            // r.setOutlineThickness(2.f);
            // r.setFillColor(sf::Color::Transparent);
            // win.draw(r);
            draw_fs(x, win);
        }
    }
}


#include "tree_node.hpp"

void init_thread_poll(int n_threads);
void run_visit_on(tree_node_t & tree);
void join_all();